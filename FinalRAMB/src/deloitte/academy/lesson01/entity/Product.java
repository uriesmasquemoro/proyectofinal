package deloitte.academy.lesson01.entity;

/**
 * This class is used for product creation, send data to create the product is
 * mandatory, so that the product will have all the desired characteristics,
 * such as code, name, cost and, quantity.
 * 
 * @author rubemendoza
 *
 */
public class Product {
	private String code;
	private String name;
	private double cost;
	private int quantity;

	public Product(String code, String name, double cost, int quantity) {
		super();
		this.code = code;
		this.name = name;
		this.cost = cost;
		this.quantity = quantity;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
