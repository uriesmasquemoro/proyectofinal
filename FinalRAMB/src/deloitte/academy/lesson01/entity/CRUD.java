package deloitte.academy.lesson01.entity;

/**
 * Interface CRUD contains all the methods to affect the products
 * @author rubemendoza
 *
 */
public interface CRUD {
	public void create();
	public void read();
	public void update();
	public void delete();
}
