package deloitte.academy.lesson01.entity;

public class Sell {
	private String product;
	private double transaction;

	public Sell(String product, double transaction) {
		super();
		this.product = product;
		this.transaction = transaction;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public double getTransaction() {
		return transaction;
	}

	public void setTransaction(double transaction) {
		this.transaction = transaction;
	}

}
