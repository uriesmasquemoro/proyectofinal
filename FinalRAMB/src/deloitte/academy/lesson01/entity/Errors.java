package deloitte.academy.lesson01.entity;

/**
 * These enums storages all the possible errors that can occur during the
 * execution of the program.
 * 
 * @author rubemendoza {@value #description}
 *
 */
public enum Errors {
	FATAL("\n\nAN UNEXPECTED ERROR HAS OCCURRED\n\n"), RECOVER("\n\nAN ERROR OCCURRED BUT THE SYSTEM WAS RESTORED\n\n");

	private String description;

	private Errors(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
