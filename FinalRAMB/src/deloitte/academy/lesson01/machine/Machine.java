package deloitte.academy.lesson01.machine;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Logger;

import deloitte.academy.lesson01.entity.CRUD;
import deloitte.academy.lesson01.entity.Product;
import deloitte.academy.lesson01.entity.Sell;

public class Machine implements CRUD {
	/*
	 * Logger required to display error/alert messages to the user,
	 * java.util.logging.Logger is a must.
	 */
	private static final Logger LOGGER = Logger.getLogger(Machine.class.getName());

	/*
	 * ArrayList of Product type to storage the elements in the machine,
	 * java.util.ArrayList is a must.
	 */
	private static final ArrayList<Product> stock = new ArrayList<Product>();

	/*
	 * Array list of Sell type to storage every sold product
	 */
	private static final ArrayList<Sell> soldProducts = new ArrayList<Sell>();

	/**
	 * This method allows user to add a product to the stock
	 * 
	 * @param product it requires an element of type Product
	 */
	public static void addProduct(Product product) {
		Machine.stock.add(product);
	}

	/**
	 * This method displays products on screen and allows user to know which
	 * products are available and their cost
	 */
	public static void displayProducts() {
		System.out.println("\n\n...::: DISPLAYING PRODUCTS :::...");

		for (Product product : Machine.stock) {
			System.out.println(product.getCode() + "\t" + product.getCost() + "\t" + product.getQuantity() + "\t"
					+ product.getName());
		}

		System.out.println(":::::::::::::::::::::::::::::::::::\n\n");
	}

	/**
	 * This method allows user to buy a product, the machine changes the current
	 * quantity of the selected product
	 * 
	 * @param code to sell the product it is necessary to give a valid product code
	 */
	public static void sellProduct(String code) {
		for (Product product : Machine.stock) {
			// Verify if the product code exists
			if (product.getCode().equals(code)) {
				// Verify if there's enough product to sell
				if (product.getQuantity() > 0) {
					var current = product.getQuantity(); // get current quantity
					product.setQuantity(current - 1); // take one away from stock
					System.out.println("\n:: " + product.getName() + " bought succesfully.\n");

					// Add sold product to soldProduct list
					Machine.soldProducts.add(new Sell(product.getName(), product.getCost()));
					return;
				} else {
					// Notify user that the product cannot be sold
					LOGGER.info("\n:: " + product.getName() + " is not currently available, sorry.");
					return;
				}
			}
		}

		LOGGER.info("That product does not exist, try again.");
	}

	/**
	 * This method allows user to see every sold product, it does not require
	 * arguments
	 */
	public static void showSoldProducts() {
		System.out.println("\n\n...::: SOLD PRODUCTS :::...");

		var index = 0;
		var total = 0.0;

		for (Sell sell : Machine.soldProducts) {
			System.out.println(sell.getProduct() + " sold for $" + sell.getTransaction());
			total += sell.getTransaction();
			index++;
		}

		if (index == 0) {
			System.out.println("NOTHING SOLD");
		}

		System.out.println(":: TOTAL: $" + total);
		System.out.println(":: TOTAL DE VENTAS: " + index);
		System.out.println(":::::::::::::::::::::::::::::::::::\n\n");
	}

	/**
	 * This method allows user to create a new product and add to the stock
	 */
	@Override
	public void create() {
		System.out.println(":: CREATING PRODUCT");

		/*
		 * The machine is going to ask the user to introduce the product code, so it can
		 * be updated
		 */
		Scanner sc = new Scanner(System.in);

		System.out.print("Plese type the product code: ");
		String code = sc.nextLine().toUpperCase();
		
		if(!validateCode(code)) {
			return;
		}
		
		System.out.print("Plese type the product name: ");
		String name = sc.nextLine().toUpperCase();
		
		if(!validateName(name)) {
			return;
		}
		
		System.out.print("Plese type the product cost: ");
		double cost = Double.parseDouble(sc.nextLine());
		System.out.print("Plese type the product quantity: ");
		int quantity = Integer.parseInt(sc.nextLine());

		Product product = new Product(code, name, cost, quantity);

		Machine.stock.add(product);

		System.out.println("\n\n:: PRODUCT SUCCESSFULLY CREATED\n\n");
	}

	/**
	 * This method allows user to know product data given its name.
	 * 
	 * @param productName this is required because the machine will try to find the
	 *                    product by its name
	 */
	@Override
	public void read() {
		/*
		 * The machine is going to ask the user to introduce the product code, so it can
		 * be updated
		 */
		Scanner sc = new Scanner(System.in);

		System.out.print("Plese type the product name: ");
		/*
		 * Upper casing the string will allow user to type lower/upper case without
		 * throwing an error
		 */
		String productName = sc.nextLine().toUpperCase();

		for (Product product : Machine.stock) {
			if (product.getName().equals(productName)) {
				System.out.println(":: PRODUCT INFO\n");
				System.out.println("THERE ARE " + product.getQuantity() + " " + product.getCode() + "-"
						+ product.getName() + "(S) IN THE MACHINE");
				return;
			}
		}

		LOGGER.info("\n\nTHAT PRODUCT SEEMS TO BE MISSING IN THE MACHINE\n\n");
	}

	/**
	 * This method allows user to update a product given its product code
	 */
	@Override
	public void update() {
		System.out.println(":: UPDATING PRODUCT");
		/*
		 * The machine is going to ask the user to introduce the product code, so it can
		 * be updated
		 */
		Scanner sc = new Scanner(System.in);

		System.out.print("Please type a product code: ");
		String productCode = sc.nextLine().toUpperCase();

		var index = 0; // if product is found then the index will be used to update the product

		for (Product product : Machine.stock) {
			if (product.getCode().equals(productCode)) {
				System.out.print("Please type the new product name: ");
				String name = sc.nextLine().toUpperCase();
				
				if(!validateName(name)) {
					return;
				}

				System.out.print("Please type the new product cost: ");
				double cost = Double.parseDouble(sc.nextLine());

				System.out.print("Please type the new product quantity: ");
				int quantity = Integer.parseInt(sc.nextLine());

				Machine.stock.get(index).setName(name);
				Machine.stock.get(index).setCost(cost);
				Machine.stock.get(index).setQuantity(quantity);

				System.out.println("\n\n:: PRODUCT SUCCESSFULLY UPDATED\n\n");
				return;
			}

			index++;
		}

		LOGGER.info("THAT PRODUCT DOES NOT EXIST");
	}

	/**
	 * This method allows user to delete an specific item from the list of products (given a product code)
	 */
	@Override
	public void delete() {
		// TODO Auto-generated method stub
		System.out.println(":: DELETING PRODUCT");
		/*
		 * The machine is going to ask the user to introduce the product code, so it can
		 * be updated
		 */
		Scanner sc = new Scanner(System.in);

		System.out.print("Please type a product code: ");
		String productCode = sc.nextLine().toUpperCase();

		var index = 0; // if product is found then the index will be used to update the product

		for (Product product : Machine.stock) {
			if (product.getCode().equals(productCode)) {
				Machine.stock.remove(index);

				System.out.println("\n\n:: PRODUCT SUCCESSFULLY DELETED\n\n");
				return;
			}

			index++;
		}

		LOGGER.info("THAT PRODUCT DOES NOT EXIST");
	}

	/**
	 * This method validates the name of a product, if it returns true is because
	 * the name is valid
	 * 
	 * @param name is required to validate if the user is typing a valid name
	 * @return a boolean type value
	 */
	public static Boolean validateName(String name) {
		for (Product p : Machine.stock) {
			if(p.getName().equals(name)) {
				LOGGER.info("\n\n:: THAT IS NOT A VALID NAME, TRY AGAIN!\n\n");
				return false;
			}
		}

		return true;
	}
	
	/**
	 * This method validates the code of a product, if it returns true is because
	 * the code is valid
	 * 
	 * @param code is required to validate if the user is typing a valid code
	 * @return a boolean type value
	 */
	public static Boolean validateCode(String code) {
		for (Product p : Machine.stock) {
			if(p.getCode().equals(code)) {
				LOGGER.info("\n\n:: THAT IS NOT A VALID CODE, TRY AGAIN!\n\n");
				return false;
			}
		}

		return true;
	}
}
