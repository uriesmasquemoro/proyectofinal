package deloitte.academy.lesson01.run;

import deloitte.academy.lesson01.entity.Menu;

/**
 * This is the Main class, where all the logic of the program is executed, it
 * avoids having everything on the main method, it just calls another class that
 * already does that.
 * 
 * {@link deloitte.academy.lesson01.entity.Menu}
 * 
 * @author rubemendoza heisramby@gmail.com
 * @version: 0.1
 * @since 03-11-2020
 *
 */
public class Main {

	/**
	 * This is the Main method, it simply calls the showMenu method from Menu class.
	 * 
	 * @param args arguments are not required in this class
	 */
	public static void main(String[] args) {
		Menu.showMenu();
	}

}
